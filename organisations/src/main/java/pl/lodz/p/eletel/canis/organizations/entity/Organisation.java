package pl.lodz.p.eletel.canis.organizations.entity;

import javax.persistence.*;

@Entity
public class Organisation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    @Column(name = "facultyShortName")
    String facultyShortName;

    @Column(name = "facultyFullName")
    String facultyFullName;

    @Column(name = "facultyLogo")
    String facultyLogo;

    public Organisation(Integer id, String facultyShortName, String facultyFullName, String facultyLogo) {
        this.id = id;
        this.facultyShortName = facultyShortName;
        this.facultyFullName = facultyFullName;
        this.facultyLogo = facultyLogo;
    }

    public Organisation(){
        this(-1, null, null, null);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFacultyShortName() {
        return facultyShortName;
    }

    public void setFacultyShortName(String facultyShortName) {
        this.facultyShortName = facultyShortName;
    }

    public String getFacultyFullName() {
        return facultyFullName;
    }

    public void setFacultyFullName(String facultyFullName) {
        this.facultyFullName = facultyFullName;
    }

    public String getFacultyLogo() {
        return facultyLogo;
    }

    public void setFacultyLogo(String facultyLogo) {
        this.facultyLogo = facultyLogo;
    }
}

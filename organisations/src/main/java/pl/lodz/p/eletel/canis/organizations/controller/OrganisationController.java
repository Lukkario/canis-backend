package pl.lodz.p.eletel.canis.organizations.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.lodz.p.eletel.canis.organizations.entity.Organisation;
import pl.lodz.p.eletel.canis.organizations.repository.OrganisationRepository;

import java.util.List;
import java.util.Optional;

@RestController
public class OrganisationController {
    @Autowired
    private OrganisationRepository organisationRepository;

    @GetMapping(value = "/api/organisations/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findOrganisation(@RequestBody(required = false) Organisation organisation){
        if(organisation != null) {
            if(organisation.getFacultyShortName() == null && organisation.getFacultyFullName() == null)
                return new ResponseEntity<>(organisationRepository.findAll(), HttpStatus.OK);
            Optional<Organisation> foundOrganisation;
            if(organisation.getFacultyFullName() != null){
                foundOrganisation = organisationRepository.findOrganisationByfacultyFullName(organisation.getFacultyFullName());
                if(foundOrganisation.isPresent())
                    return new ResponseEntity<>(foundOrganisation.get(), HttpStatus.OK);
                return new ResponseEntity<>(new Organisation(), HttpStatus.NOT_FOUND);
            }
            else if(organisation.getFacultyShortName() != null){
                foundOrganisation = organisationRepository.findOrganisationByfacultyShortName(organisation.getFacultyShortName());
                if(foundOrganisation.isPresent())
                    return new ResponseEntity<>(foundOrganisation.get(), HttpStatus.OK);
                return new ResponseEntity<>(new Organisation(), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(new Organisation(), HttpStatus.NOT_FOUND);
        } else {
         return new ResponseEntity<>(organisationRepository.findAll(), HttpStatus.OK);
        }
    }

    @GetMapping(value = "/api/organisations/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Organisation> findOrganisationById(@PathVariable("id") Integer id){
        Optional<Organisation> foundOrganisation = organisationRepository.findById(id);
        if(foundOrganisation.isPresent())
            return new ResponseEntity<>(foundOrganisation.get(), HttpStatus.OK);
        return new ResponseEntity<>(new Organisation(), HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/api/organisations/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addOrganisation(@RequestBody(required = true) Organisation organisation){
        if(organisation.getFacultyShortName() == null || organisation.getFacultyFullName() == null || organisation.getFacultyLogo() == null)
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        if(organisationRepository.findOrganisationByfacultyShortNameAndFacultyFullName(organisation.getFacultyShortName(), organisation.getFacultyFullName()).isPresent())
            return new ResponseEntity(HttpStatus.CONFLICT);
        organisation.setId(-1);
        organisationRepository.save(organisation);
        return new ResponseEntity(HttpStatus.OK);
    }
}

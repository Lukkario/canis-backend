package pl.lodz.p.eletel.canis.organizations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrganisationsApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrganisationsApplication.class, args);
    }
}

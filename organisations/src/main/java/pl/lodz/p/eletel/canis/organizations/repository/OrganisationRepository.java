package pl.lodz.p.eletel.canis.organizations.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.lodz.p.eletel.canis.organizations.entity.Organisation;

import java.util.Optional;

public interface OrganisationRepository extends JpaRepository<Organisation, Integer> {
    Optional<Organisation> findOrganisationByfacultyShortName(String facultyShortName);
    Optional<Organisation> findOrganisationByfacultyFullName(String facultyFullName);
    Optional<Organisation> findOrganisationByfacultyShortNameAndFacultyFullName(String facultyShortName, String facultyFullName);
}

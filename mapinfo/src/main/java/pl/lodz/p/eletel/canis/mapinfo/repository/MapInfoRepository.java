package pl.lodz.p.eletel.canis.mapinfo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.lodz.p.eletel.canis.mapinfo.entity.MapInfo;

@Repository
public interface MapInfoRepository extends CrudRepository<MapInfo, Integer> {
}

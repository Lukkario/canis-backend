package pl.lodz.p.eletel.canis.mapinfo.entity;

import javax.persistence.*;

@Entity
public class MapInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    @Column(length = 1024)
    private String shortInfo;
    @Column(length = 8192)
    private String fullInfo;
    @Column(length = 512)
    private String imageURL;
    private Integer beaconId;

    public MapInfo() {
        this(null, null, null, null, null, null);
    }

    public MapInfo(Integer id, String title, String shortInfo, String fullInfo, String imageURL, Integer beaconId) {
        this.id = id;
        this.title = title;
        this.shortInfo = shortInfo;
        this.fullInfo = fullInfo;
        this.imageURL = imageURL;
        this.beaconId = beaconId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortInfo() {
        return shortInfo;
    }

    public void setShortInfo(String shortInfo) {
        this.shortInfo = shortInfo;
    }

    public String getFullInfo() {
        return fullInfo;
    }

    public void setFullInfo(String fullInfo) {
        this.fullInfo = fullInfo;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Integer getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(Integer beaconId) {
        this.beaconId = beaconId;
    }
}

package pl.lodz.p.eletel.canis.mapinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapInfoApplication.class, args);
    }

}

package pl.lodz.p.eletel.canis.mapinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import pl.lodz.p.eletel.canis.mapinfo.entity.MapInfo;
import pl.lodz.p.eletel.canis.mapinfo.repository.MapInfoRepository;

import javax.annotation.security.RolesAllowed;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
public class MapInfoController {

    @Autowired
    private MapInfoRepository mapInfoRepository;

    @GetMapping("/api/mapinfo/")
    public Iterable<MapInfo> getAll() {
        return mapInfoRepository.findAll();
    }

    @GetMapping("/api/mapinfo/ids")
    public Iterable<Integer> getAllIds() {
        Iterable<MapInfo> allMapInfo = mapInfoRepository.findAll();
        return StreamSupport.stream(allMapInfo.spliterator(), false).map(MapInfo::getId).collect(Collectors.toList());
    }

    @GetMapping("/api/mapinfo/{id}")
    public MapInfo getById(@PathVariable("id") Integer id) {
        Optional mapInfoHandle = mapInfoRepository.findById(id);
        if(mapInfoHandle.isPresent()) {
            return (MapInfo) mapInfoHandle.get();
        } else {
            return new MapInfo();
        }
    }

    @PostMapping("/api/mapinfo/")
    public MultiValueMap<String, String> addNew(@RequestBody MapInfo mapInfo) {
        mapInfo.setId(null);
        MapInfo resultMapInfo = mapInfoRepository.save(mapInfo);
        MultiValueMap<String, String> result = new LinkedMultiValueMap<>();

        result.add("id", resultMapInfo.getId().toString());
        return result;
    }

}

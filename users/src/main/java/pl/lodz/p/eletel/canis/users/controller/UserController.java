package pl.lodz.p.eletel.canis.users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.lodz.p.eletel.canis.users.entity.User;
import pl.lodz.p.eletel.canis.users.repository.UserRepository;

import java.security.Principal;

@RestController
public class UserController {
    @Autowired
    private UserRepository userRepository;

    //TODO: Add user to database if administartor
    @PostMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addUser(@RequestBody User user)
    {
        userRepository.save(user);
    }

    @GetMapping(value = "/user")
    public User getUser(Principal principal)
    {
        if(principal == null)
            return new User(null, null, null);

        User user = userRepository.getUserByGoogleID(principal.getName());

        if(user != null)
            return user;

        return new User(null, null, null);
    }

    @GetMapping(value = "/")
    public void login(Principal principal)
    {
        if(principal == null)
            return;
        User user = userRepository.getUserByGoogleID(principal.getName());
        if(user == null)
        {
            user = new User(null, principal.getName(), "GUEST");
            userRepository.save(user);
        }
    }
}

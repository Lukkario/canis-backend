package pl.lodz.p.eletel.canis.users.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    //@JsonIgnore
    Integer id;

    @Column(name = "googleID")
    String googleID;

    @Column(name = "role")
    String role;

    public User(Integer id, String googleID, String role) {
        this.id = id;
        this.googleID = googleID;
        this.role = role;
    }

    public User() {
        this(-1,  null, null);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoogleID() {
        return googleID;
    }

    public void setGoogleID(String googleID) {
        this.googleID = googleID;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

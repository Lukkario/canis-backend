package pl.lodz.p.eletel.canis.users.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.lodz.p.eletel.canis.users.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User getUserByGoogleID(String googleID);
}

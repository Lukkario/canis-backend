# Canis - backend
[![pipeline status](https://gitlab.com/Lukkario/canis-backend/badges/master/pipeline.svg)](https://gitlab.com/Lukkario/canis-backend/commits/master)
[![coverage report](https://gitlab.com/Lukkario/canis-backend/badges/master/coverage.svg)](https://gitlab.com/Lukkario/canis-backend/commits/master)  
Backend for "Canis" application, used for navigating around Lodz University of Technology.  
Based on microservices and containerization.

## Getting Started  

These instructions will get you a copy of the project up and
running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.  

### Prerequisites

* Java/OpenJDK 8
* Maven >= 3
* Docker

### Installing

First, grab a copy of repository from GitLab and change the working directory:
```
$ git clone https://gitlab.com/Lukkario/canis-backend.git
$ cd canis-backend
```

Then, use Maven to build JAR files for Docker:  
```
$ mvn clean install
```
When build is done, use Docker Compose to build the image:
```
$ docker-compose build
```
## Deployment

This application is designed to be run within containers.  
Just use Docker Compose within working directory:
```
$ docker-compose up
```

## Contributing

First, create a Maven module (if using IntelliJ IDEA):  
![Creating module with IntelliJ](https://i.imgur.com/fb3mKLZ.png)  
  
  
If you're not using IntelliJ IDEA, create new directory and create pom.xml:
```
$ mkdir core-newmodule
$ cd core-newmodule
$ touch pom.xml
```
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>pl.lodz.p.eletel.telin.canis</groupId>
        <artifactId>backend</artifactId>
        <version>0.0</version>
    </parent>

    <artifactId>core-newmodule</artifactId>
    <name>core-newmodule</name>
    <description>New microservice for "Canis"</description>

    <developers>
        <developer>
            <id>YourUsername</id>
            <name>John Doe</name>
            <email>johndoe@canis.pl</email>
        </developer>
    </developers>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>2.1.3.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
            <version>2.1.1.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <version>2.1.4.RELEASE</version>
            <optional>true</optional>
        </dependency>
    </dependencies>


    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>


    <!-- use this if you want test your service before building Docker image -->
    <properties>
        <start-class>pl.lodz.p.eletel.canis.NewModuleApplication</start-class>
    </properties>

</project>

```  
Add line to `<modules>` tag in root pom.xml:
```xml
<modules>
    <module>core-eureka-server</module>
    <module>core-zuul-gateway</module>
    <module>misc-version</module>
    .
    .
    .
    <module>core-newmodule</module>
</modules>
```
This makes module buildable with `mvn clean install`.  

Next, add `Dockerfile` in root directory with following content:  
```Dockerfile
FROM java:8
VOLUME /tmp
ADD target/core-newmodule-0.0.jar core-newmodule.jar
RUN bash -c 'touch /core-newmodule.jar'
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/core-newmodule.jar"]
```

Now, (**you need to do this step regardless of using IntelliJ!**), add new entry to docker-compose.yml:  
```yaml
core-newmodule:
  container_name: "core-newmodule"
  build: ./core-newmodule
  depends_on:
    - "core-eureka-server"
  environment:
    - EUREKA_SERVER_ADDRESS=core-eureka-server
    - EUREKA_SERVER_PORT=8761
    - BASE_PORT=xxxx
  networks:
    - "canis"
```  
Module should now be visible by other microservices and within API.  
Last, you need to add mapping in `core-zuul-gateway`.  
1. Go to the `core-zuul-gateway/src/main/java/resources/application.resources`

1. Append following line to `application.resources`.
```
zuul.routes.misc-version.path=/newmodule/**
zuul.routes.misc-version.service-id=core-newmodule
```
Service should now be accessible under URL `http://CANIS-URL/api/newmodule/`.

## Built with
* [Spring Boot](https://github.com/spring-projects/spring-boot) - Java web framework used as base for microservice development  
* [Eureka](https://github.com/Netflix/eureka) - Service registry telling which services are running  
* [Zuul](https://github.com/Netflix/zuul) - Gateway to API handled by distributed services running within Docker
  
## Authors
* **Sebastian Madejski** - core architecture - [mwht](https://github.com/mwht)
* **Michał Łukiański** - [Lukkario](https://gitlab.com/Lukkario) 
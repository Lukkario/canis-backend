package pl.lodz.p.eletel.telin.canis.navplaces;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NavPlacesApplication {

    public static void main(String[] args) {
        SpringApplication.run(NavPlacesApplication.class, args);
    }

}

package pl.lodz.p.eletel.telin.canis.navplaces.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.lodz.p.eletel.telin.canis.navplaces.model.NavPlace;
import pl.lodz.p.eletel.telin.canis.navplaces.repository.NavPlaceRepository;

import javax.servlet.http.HttpServletResponse;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
public class IndexController {

    @Autowired
    private NavPlaceRepository navPlaceRepository;

    @GetMapping("/api/navplaces/")
    public Iterable<NavPlace> index() {
        return navPlaceRepository.findAll();
    }

    @GetMapping("/api/navplaces/ids")
    public Iterable<Integer> getAllIds() {
        Iterable<NavPlace> allMapInfo = navPlaceRepository.findAll();
        return StreamSupport.stream(allMapInfo.spliterator(), false).map(NavPlace::getId).collect(Collectors.toList());
    }

    @GetMapping("/api/navplaces/{id}")
    public NavPlace getSingleNavPlace(@PathVariable("id") Integer id, HttpServletResponse httpServletResponse) {
        Optional<NavPlace> navPlace = navPlaceRepository.findById(id);
        try {
            return navPlace.get();
        } catch (NoSuchElementException e) {
            Logger.getLogger(this.getClass().getName()).severe(e.toString());
            httpServletResponse.setStatus(404);
            return null;
        }
    }

    @PostMapping(path = "/api/navplaces/", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public NavPlace addNavPlace(@RequestBody NavPlace navPlace, HttpServletResponse httpServletResponse) {
        try {
            navPlace.setId(null);
            navPlace = navPlaceRepository.save(navPlace);
            return navPlace;
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).severe(e.toString());
            httpServletResponse.setStatus(500);
            return null;
        }
    }
}

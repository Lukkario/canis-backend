package pl.lodz.p.eletel.telin.canis.navplaces.repository;

import org.springframework.data.repository.CrudRepository;
import pl.lodz.p.eletel.telin.canis.navplaces.model.NavPlace;

public interface NavPlaceRepository extends CrudRepository<NavPlace, Integer> {
}

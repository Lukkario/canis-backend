package pl.lodz.p.eletel.canis.workers.entity;

import javax.persistence.*;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    @Column(name = "first_name")
    String firstName;

    @Column(name = "last_name")
    String lastName;

    @Column(name = "hours")
    String hours;

    @Column(name = "address")
    String address;

    @Column(name = "contact")
    String contact;

    @Column(name = "photo")
    String photo;

    public Employee(Integer id, String name, String surname, String hours, String address, String contact, String photo) {
        this.id = id;
        this.firstName = name;
        this.lastName = surname;
        this.hours = hours;
        this.address = address;
        this.contact = contact;
        this.photo = photo;
    }

    public Employee() {
        this(-1,null,null,null,null,null, null);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}

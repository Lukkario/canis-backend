package pl.lodz.p.eletel.canis.workers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.lodz.p.eletel.canis.workers.entity.Employee;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    List<Employee> findEmployeeByfirstName(String firstName);
    List<Employee> findEmployeeBylastName(String lastName);
    List<Employee> findEmployeeByFirstNameAndLastName(String fistName, String lastName);
}

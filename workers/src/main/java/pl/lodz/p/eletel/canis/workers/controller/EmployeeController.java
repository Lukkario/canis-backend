package pl.lodz.p.eletel.canis.workers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.lodz.p.eletel.canis.workers.entity.Employee;
import pl.lodz.p.eletel.canis.workers.repository.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping(value = "/api/workers/ids")
    public ResponseEntity<List<Integer>> sendEmployeesIndex() {
        return new ResponseEntity<>(employeeRepository.findAll().stream().map(e -> e.getId()).collect(Collectors.toList()), HttpStatus.OK);
    }

    @GetMapping(value = "/api/workers/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> findEmployee(@RequestBody(required = false) Employee employee){
        if(employee == null)
            return new ResponseEntity<>(employeeRepository.findAll(), HttpStatus.OK);
        List<Employee> employees;
        if(employee.getFirstName() != null && employee.getLastName() != null)
        {
            employees = employeeRepository.findEmployeeByFirstNameAndLastName(employee.getFirstName(), employee.getLastName());
            return employees.isEmpty() ? new ResponseEntity<>(employees, HttpStatus.NOT_FOUND) : new ResponseEntity<>(employees, HttpStatus.OK);
        }
        else if(employee.getLastName() != null)
        {
            employees = employeeRepository.findEmployeeBylastName(employee.getLastName());
            return employees.isEmpty() ? new ResponseEntity<>(employees, HttpStatus.NOT_FOUND) : new ResponseEntity<>(employees, HttpStatus.OK);
        }
        else if(employee.getFirstName() != null)
        {
            employees = employeeRepository.findEmployeeByfirstName(employee.getFirstName());
            return employees.isEmpty() ? new ResponseEntity<>(employees, HttpStatus.NOT_FOUND) : new ResponseEntity<>(employees, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/api/workers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> findEmployeeById(@PathVariable("id") Integer id){
        Optional<Employee> employee = employeeRepository.findById(id);
        if(employee.isPresent())
            return new ResponseEntity<>(employee.get(), HttpStatus.OK);
        return new ResponseEntity<>(new Employee(), HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/api/workers/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addEmployee(@RequestBody Employee employee) {
        if(employee.getFirstName() == null || employee.getLastName() == null)
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        if(!employeeRepository.findEmployeeByFirstNameAndLastName(employee.getFirstName(), employee.getLastName()).isEmpty())
            return new ResponseEntity(HttpStatus.CONFLICT);
        employee.setId(null);
        employeeRepository.save(employee);
        return new ResponseEntity(HttpStatus.CREATED);
    }


    @PutMapping(value = "/api/workers", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateEmployee(@RequestBody Employee employee) {
        if(employee.getFirstName() == null || employee.getLastName() == null)
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        Optional<Employee> updatedEmployee = employeeRepository.findById(employee.getId());
        if(!updatedEmployee.isPresent())
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        if (employee.getAddress() != null)
            updatedEmployee.get().setAddress(employee.getAddress());
        if (employee.getContact() != null)
            updatedEmployee.get().setContact(employee.getContact());
        if (employee.getHours() != null)
            updatedEmployee.get().setHours(employee.getHours());
        if (employee.getPhoto() != null)
            updatedEmployee.get().setPhoto(employee.getPhoto());

        employeeRepository.save(updatedEmployee.get());
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}

package pl.lodz.p.eletel.canis.misc.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.lodz.p.eletel.canis.misc.model.Version;

@RestController
public class VersionController {

    @Value("${canis.version.vcsId}")
    private String vcsId;

    @Value("${canis.version.vcsDate}")
    private String vcsDate;

    @Value("${canis.version.vcsBranch}")
    private String vcsBranch;

    @Value("${canis.version.buildDate}")
    private String buildDate;

    @GetMapping("/")
    public Version getCurrentVersion() {
        return new Version(vcsId, vcsBranch, vcsDate, buildDate);
    }

}

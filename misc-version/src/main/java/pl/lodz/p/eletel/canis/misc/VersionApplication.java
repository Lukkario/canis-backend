package pl.lodz.p.eletel.canis.misc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class VersionApplication {
    public static void main(String[] args) {
        SpringApplication.run(VersionApplication.class, args);
    }
}

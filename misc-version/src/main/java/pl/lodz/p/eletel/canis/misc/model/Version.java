package pl.lodz.p.eletel.canis.misc.model;

public class Version {
    private String vcsId;
    private String vcsBranch;
    private String vcsDate;
    private String buildDate;

    public Version(String vcsId, String vcsBranch, String vcsDate, String buildDate) {
        this.vcsId = vcsId;
        this.vcsBranch = vcsBranch;
        this.vcsDate = vcsDate;
        this.buildDate = buildDate;
    }

    public String getVcsId() {
        return vcsId;
    }

    public String getVcsBranch() {
        return vcsBranch;
    }

    public String getVcsDate() {
        return vcsDate;
    }

    public String getBuildDate() {
        return buildDate;
    }
}
